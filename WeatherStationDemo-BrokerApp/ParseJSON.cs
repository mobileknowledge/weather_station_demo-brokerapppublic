﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherStationDemo_CloudContracts;

namespace WeatherStationDemo_CloudApp.Parser
{
    public class ParseJSON
    {
        public static RapidIoTDevice parseMeasurementJSON(string json)
        {
            if (!string.Equals("/will", json)) {
                RapidIoTDevice wsdObjectReceived = JsonConvert.DeserializeObject<RapidIoTDevice>(json);
                return wsdObjectReceived;
            }
            else {
                return null;
            }
        }

        public static ClearCloud parseDatabaseClearJSON(string json)
        {
            ClearCloud databaseObjectReceived = JsonConvert.DeserializeObject<ClearCloud>(json);
            return databaseObjectReceived;
        }
    }
}