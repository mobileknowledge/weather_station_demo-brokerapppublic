﻿using System;
using WeatherStationDemo_CloudApp.Database;
using WeatherStationDemo_CloudApp.Parser;
using System.Threading;
using Apache.NMS;
using Apache.NMS.ActiveMQ;

namespace RapidIoT_CloudBroker
{
    public class RapidIoTBroker
    { 
        //Create a listener variable to subscribe the broker to the three topics in the ActiveMQ broker (Android, iOS, ClearDB)
        static Listener listener;

        //This variable controls if the Threads to subscribe the topics are active, in case an exception is raised it creates again the
        //subscriber to the broker so the broker is available at any time.
        static private bool registerThreads;

        static void Main(string[] args)
        {
            registerThreads = true;

            //Create an infinite loop to check continuously if the broker is subscribed to the server.
            while (true)
            {
                try
                {
                    if (registerThreads)
                    {
                        registerThreads = false;

                        listener = new Listener();

                        //Create new threads to register to the topics for a better distribution of computing workload.
                        new Thread(() =>
                        {
                            listener.registerIOSTopic();
                        }).Start();

                        new Thread(() =>
                        {
                            listener.registerAndroidTopic();
                        }).Start();

                        new Thread(() =>
                        {
                            listener.registerClearDBTopic();
                        }).Start();
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Exception raised: " + e);
                    registerThreads = true;
                }
            }
        }

        public class Listener
        {
            //Topics created in ActiveMQ instance
            public const string IOS_DESTINATION_TOPIC = "weather-station-iOS";
            public const string ANDROID_DESTINATION_TOPIC = "weather-station-android";
            public const string CLEARDB_DESTINATION_TOPIC = "weather-station-clear-database";

            IConnectionFactory connectionFactory;
            IConnection _connection;

            public Listener()
            {
                try
                {
                    //Create a connection to the Amazon Web Services endpoint with the broker application
                    connectionFactory =
                        new ConnectionFactory(BrokerAccountInfo.AWS_WSD_ENDPOINT);
                    _connection = connectionFactory.CreateConnection(BrokerAccountInfo.ACTIVEMQ_USERNAME,
                        BrokerAccountInfo.ACTIVEMQ_PASSWORD);
                    _connection.Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.WriteLine("Could not start listener. Press <ENTER> to exit.");
                    Console.Read();
                }
            }

            public void registerIOSTopic()
            {
                //Create a new session to subscribe to the iOS topic.
                ISession _session = _connection.CreateSession();
                IDestination dest = _session.GetTopic(IOS_DESTINATION_TOPIC);

                //Create a new consumer and start listening for incoming messages to the topic.
                using (IMessageConsumer consumer = _session.CreateConsumer(dest))
                {
                    Console.WriteLine("iOS Listener started.");
                    Console.WriteLine("iOS Listener created.rn");
                    IMessage message;

                    //We will continuously check for new messages in this loop
                    while (true)
                    {
                        message = consumer.Receive();

                        //If there is a new message in the topic just read and process it.
                        if (message != null)
                        {
                            var bytesMessage = message as IBytesMessage;
                            if (bytesMessage != null)
                            {
                                byte[] content = bytesMessage.Content;
                                string result = System.Text.Encoding.UTF8.GetString(content);
                                if (!string.IsNullOrEmpty(result))
                                {
                                    var receivedObject = ParseJSON.parseMeasurementJSON(result);

                                    if (receivedObject != null)
                                        DatabaseOperations.checkUserLimitAndAddMeasurementToDatabase(receivedObject);
                                }
                            }
                        }
                    }
                }
            }

            public void registerAndroidTopic()
            {
                //Create a new session to subscribe to the iOS topic.
                ISession _session = _connection.CreateSession();
                IDestination dest = _session.GetTopic(ANDROID_DESTINATION_TOPIC);

                //Create a new consumer and start listening for incoming messages to the topic.
                using (IMessageConsumer consumer = _session.CreateConsumer(dest))
                {
                    Console.WriteLine("Android Listener started.");
                    Console.WriteLine("Android Listener created.rn");
                    IMessage message;

                    //We will continuously check for new messages in this loop
                    while (true)
                    {
                        message = consumer.Receive();

                        //If there is a new message in the topic just read and process it.
                        if (message != null)
                        {
                            var bytesMessage = message as IBytesMessage;
                            if (bytesMessage != null)
                            {
                                byte[] content = bytesMessage.Content;
                                string result = System.Text.Encoding.UTF8.GetString(content);
                                if (!string.IsNullOrEmpty(result))
                                {
                                    var receivedObject = ParseJSON.parseMeasurementJSON(result);

                                    if(receivedObject != null)
                                        DatabaseOperations.checkUserLimitAndAddMeasurementToDatabase(receivedObject);
                                }
                            }
                        }
                    }
                }
            }

            public void registerClearDBTopic()
            {
                //Create a new session to subscribe to the iOS topic.
                ISession _session = _connection.CreateSession();
                IDestination dest = _session.GetTopic(CLEARDB_DESTINATION_TOPIC);

                //Create a new consumer and start listening for incoming messages to the topic.
                using (IMessageConsumer consumer = _session.CreateConsumer(dest))
                {
                    Console.WriteLine("Clear DB Listener started.");
                    Console.WriteLine("Clear DB Listener created.rn");
                    IMessage message;

                    //We will continuously check for new messages in this loop
                    while (true)
                    {
                        message = consumer.Receive();

                        //If there is a new message in the topic just read and process it.
                        if (message != null)
                        {
                            var bytesMessage = message as IBytesMessage;
                            if (bytesMessage != null)
                            {
                                byte[] content = bytesMessage.Content;
                                string result = System.Text.Encoding.UTF8.GetString(content);
                                if (!string.IsNullOrEmpty(result))
                                {
                                    //Parse the JSON with the clear database message to retrieve the corresponding Session ID that
                                    //will be used in the DatabaseOperations class.
                                    var receivedObject = ParseJSON.parseDatabaseClearJSON(result);

                                    if (receivedObject != null)
                                        DatabaseOperations.deleteMeasurementsWithSessionID(receivedObject.SessionID);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
