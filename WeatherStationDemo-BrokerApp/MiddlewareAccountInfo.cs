﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RapidIoT_CloudBroker
{
    public static class MiddlewareAccountInfo
    {
        public const string AWS_WSD_ENDPOINT = "ssl://b-a696cbdc-47a5-45be-a9ee-da0dd8f1d3b5-1.mq.us-west-2.amazonaws.com:61617";
        public const int AWS_WSD_PORT = 8883;
        public const string ACTIVEMQ_CLIENTID = "WeatherStation-CloudApp";
        public const string ACTIVEMQ_USERNAME = "admin";
        public const string ACTIVEMQ_PASSWORD = "admin1234567";
    }
}
