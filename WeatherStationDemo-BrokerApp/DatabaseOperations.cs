﻿using RapidIoT_CloudBroker;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WeatherStationDemo_CloudContracts;

namespace WeatherStationDemo_CloudApp.Database
{
    public class DatabaseOperations
    {
        //Limit the maximun number of entries per user to 12800, this number fits the 1 Mb of data required per user.
        private static int maxNumberOfEntriesPerUser = 12800;

        public static void deleteMeasurementsWithSessionID(String mSessionID)
        {
            using (SqlConnection con = new SqlConnection(Global.connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("DELETE FROM dbo.RapidIoTDevices WHERE SessionID = '" + mSessionID + "'", con))
                {
                    command.ExecuteNonQuery();
                }
                con.Close();
            }
        }

        public static void checkUserLimitAndAddMeasurementToDatabase(RapidIoTDevice rapidIoTDevice)
        {
            using (SqlConnection con = new SqlConnection(Global.connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM dbo.RapidIoTDevices " +
                    "WHERE SessionID = '" + rapidIoTDevice.bleMACAddress + "'", con))
                {
                    Int32 count = (Int32)command.ExecuteScalar();
                    if (count >= maxNumberOfEntriesPerUser)
                    {
                        var deleteCommand = new SqlCommand("DELETE TOP 1 FROM dbo.RapidIoTDevices WHERE bleMACAddress = '" +
                            rapidIoTDevice.bleMACAddress + "'", con);

                        deleteCommand.ExecuteNonQuery();
                    }

                    if(rapidIoTDevice.SessionID != null)
                    {
                        SqlCommand myCommand = new SqlCommand("INSERT INTO dbo.RapidIoTDevices (Name, SessionID, Measurement_Temperature," +
                       "Measurement_Humidity,Measurement_Pressure,Measurement_Light,Measurement_Timestamp,bleMACAddress) " +
                                        "Values (@Name, @SessionID,@Meas_Temp,@Meas_Humid,@Meas_Pres,@Meas_Light,@Meas_Time,@bleMACAddress)"
                                        , con);
                        myCommand.Parameters.AddWithValue("@Name", rapidIoTDevice.Name);
                        myCommand.Parameters.AddWithValue("@SessionID", rapidIoTDevice.SessionID);
                        myCommand.Parameters.AddWithValue("@Meas_Temp", rapidIoTDevice.Measurement.Temperature);
                        myCommand.Parameters.AddWithValue("@Meas_Humid", rapidIoTDevice.Measurement.Humidity);
                        myCommand.Parameters.AddWithValue("@Meas_Pres", rapidIoTDevice.Measurement.Pressure);
                        myCommand.Parameters.AddWithValue("@Meas_Light", rapidIoTDevice.Measurement.Light);
                        myCommand.Parameters.AddWithValue("@Meas_Time", rapidIoTDevice.Measurement.Timestamp);
                        myCommand.Parameters.AddWithValue("@bleMACAddress", rapidIoTDevice.bleMACAddress);
                        myCommand.ExecuteNonQuery();
                    }
                    
                }
                con.Close();
            }
        }
    }
}