﻿using Newtonsoft.Json;
using WeatherStationDemo_CloudContracts;

namespace WeatherStationDemo_CloudApp.Parser
{
    //The two methods included in the ParseJSON class receive json objects (format of these JSON objects can be found in the mobile
    //applications developer guides), the outcome of the methods is an object with the JSON fields included.
    public class ParseJSON
    {
        public static RapidIoTDevice parseMeasurementJSON(string json)
        {
            //We need to check that the will message is not introduced here as would raise an exception since it does not include any measure
            if (!string.Equals("/will", json)) {
                RapidIoTDevice wsdObjectReceived = JsonConvert.DeserializeObject<RapidIoTDevice>(json);
                return wsdObjectReceived;
            }
            else {
                return null;
            }
        }

        public static ClearCloud parseDatabaseClearJSON(string json)
        {
            ClearCloud databaseObjectReceived = JsonConvert.DeserializeObject<ClearCloud>(json);
            return databaseObjectReceived;
        }
    }
}