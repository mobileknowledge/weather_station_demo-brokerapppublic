﻿using RapidIoT_CloudBroker;
using System;
using System.Data.SqlClient;
using WeatherStationDemo_CloudContracts;

namespace WeatherStationDemo_CloudApp.Database
{
    public class DatabaseOperations
    {
        //Limit the maximun number of entries per user to 12800, this number fits the 1 Mb of data required per user.
        private static int maxNumberOfEntriesPerUser = 12800;

        //The Session ID is included as parameter, we need to create an SQL query to delete from the devices table all the measurements
        //with the Session ID introduced as parameter.
        public static void deleteMeasurementsWithSessionID(String mSessionID)
        {
            //Create a connection with the broker messager
            using (SqlConnection con = new SqlConnection(BrokerAccountInfo.connectionString))
            {
                con.Open();
                using (SqlCommand deleteCommand = new SqlCommand("DELETE FROM dbo.RapidIoTDevices WHERE SessionID = '" + mSessionID + "'", con))
                {
                    deleteCommand.ExecuteNonQuery();
                }
                con.Close();
            }
        }

        //Before storing the incoming measurement we need to check if the user has reached its maximum storage size,
        //to check this we count the number of entries in the database for that Session ID, in case the max. limit is not
        //exceeded, we proceed to create a new query and store the measurement in the database.
        public static void checkUserLimitAndAddMeasurementToDatabase(RapidIoTDevice rapidIoTDevice)
        {
            using (SqlConnection con = new SqlConnection(BrokerAccountInfo.connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM dbo.RapidIoTDevices " +
                    "WHERE SessionID = '" + rapidIoTDevice.bleMACAddress + "'", con))
                {
                    //Get the number of entries in Int32 format.
                    Int32 count = (Int32)command.ExecuteScalar();

                    //In case the Max. number of entries is exceeeded we delete the last measurement and store the new received measurement
                    if (count >= maxNumberOfEntriesPerUser)
                    {
                        var deleteCommand = new SqlCommand("DELETE TOP 1 FROM dbo.RapidIoTDevices WHERE bleMACAddress = '" +
                            rapidIoTDevice.bleMACAddress + "'", con);

                        deleteCommand.ExecuteNonQuery();
                    }

                    if(rapidIoTDevice.SessionID != null)
                    {
                        SqlCommand insertCommand = new SqlCommand("INSERT INTO dbo.RapidIoTDevices (Name, SessionID, Measurement_Temperature," +
                       "Measurement_Humidity,Measurement_Pressure,Measurement_Light,Measurement_Timestamp,bleMACAddress) " +
                                        "Values (@Name, @SessionID,@Meas_Temp,@Meas_Humid,@Meas_Pres,@Meas_Light,@Meas_Time,@bleMACAddress)"
                                        , con);
                        insertCommand.Parameters.AddWithValue("@Name", rapidIoTDevice.Name);
                        insertCommand.Parameters.AddWithValue("@SessionID", rapidIoTDevice.SessionID);
                        insertCommand.Parameters.AddWithValue("@Meas_Temp", rapidIoTDevice.Measurement.Temperature);
                        insertCommand.Parameters.AddWithValue("@Meas_Humid", rapidIoTDevice.Measurement.Humidity);
                        insertCommand.Parameters.AddWithValue("@Meas_Pres", rapidIoTDevice.Measurement.Pressure);
                        insertCommand.Parameters.AddWithValue("@Meas_Light", rapidIoTDevice.Measurement.Light);
                        insertCommand.Parameters.AddWithValue("@Meas_Time", rapidIoTDevice.Measurement.Timestamp);
                        insertCommand.Parameters.AddWithValue("@bleMACAddress", rapidIoTDevice.bleMACAddress);
                        insertCommand.ExecuteNonQuery();
                    }
                    
                }
                con.Close();
            }
        }
    }
}