﻿namespace WeatherStationDemo_CloudContracts
{
    public class RapidIoTDevice
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string bleMACAddress { get; set; }
        public string SessionID { get; set; }
        public Measurement Measurement { get; set; }
    }
}
