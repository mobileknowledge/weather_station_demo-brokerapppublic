﻿namespace WeatherStationDemo_CloudContracts
{
    public class Measurement
    {
        public float Temperature { get; set; }
        public float Humidity { get; set; }
        public float Pressure { get; set; }
        public float Light { get; set; }
        public long Timestamp { get; set; }
    }
}
